import '../index.html';
import 'mapbox-gl/dist/mapbox-gl.css';
import '../reset.css';
import '../style.css';
import { LocalEvent } from './LocalEvent';
import appConfig from '../../app.config';

const mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');

class App {
    domTitleEvent;
    domDescription;
    domStartDate;
    domEndDate;
    domLongitude;
    domLatitude;
    domBtnNewAdd;
    domBtnUpdate;
    domContainer;
    domList;
    map;

    allEvents = [];

/*************************************************************************************************************/
    
    start (){
        console.log( 'Application started' );
        this.initHtml();
        this.initMapBox();
        this.loadLocalEvents();


        // // Ecouteur de clic sur la map
        this.map.on( 'click', ( evt ) => { 
            this.domLongitude.value = evt.lngLat.lng;  
            this.domLatitude.value = evt.lngLat.lat;  
        });
        
    }

/*************************************************************************************************************/

    initMapBox(){
        mapboxgl.accessToken = appConfig.mapBox.apiKey;
        this.map = new mapboxgl.Map({
            container: 'map',
            center: { lng: 2.7885292, lat: 42.6832109 },
            zoom: 10,
            customAttribution: 'Evénements Locaux',
            style: 'mapbox://styles/mapbox/streets-v11'
        });

        // Ajout d'un bouton controle du zoom
        const zoomCtrl = new mapboxgl.NavigationControl();
        this.map.addControl( zoomCtrl );
    }

/*************************************************************************************************************/

    loadLocalEvents(){
        const storageContent = localStorage.getItem( appConfig.localStorageName );
        if( storageContent === null ) {
            return;
        }

        let storedJson;

        try {
            storedJson = JSON.parse( storageContent );
        }
        //Suppression du contenu, si corrompu
        catch( error ) {
            localStorage.removeItem( appConfig.localStorageName );
            return;
        }

                //Création des événements à partir du JSON
        for( let jsonEvent of storedJson ){
            this.allEvents.push( jsonEvent );
            const localEvent = new LocalEvent( jsonEvent );

            // Ajout sur la carte
            localEvent.markerEvent.addTo( this.map );
        }      
    }

/*************************************************************************************************************/

    // Initialisation application 
    initHtml() {
        this.domTitleEvent = document.querySelector( '#title-event' );
        this.domDescription = document.querySelector( '#description' );
        this.domStartDate = document.querySelector( '#start-date' );
        this.domEndDate = document.querySelector( '#end-date' );
        this.domLongitude = document.querySelector( '#longitude' );
        this.domLatitude = document.querySelector( '#latitude' );
        this.domList= document.querySelector( '#list' );

        this.domBtnNewAdd = document.querySelector( '#new-add' );
        this.domBtnNewAdd.addEventListener( 'click', this.onBtnNewAddClick.bind(this) );
                //Sans parenthèses pour que ça appelle l'intérieur de la fonction au click 
        // Bind pour remettre la classe dans le this
    }

/***********   GESTIONNAIRE EVENEMENTS    ******************************************************/

    onErrorRemove( evt ) {
        evt.target.classList.remove( 'error' );
    }

    onBtnNewAddClick() {
        // Récupérer et les données saisies
        // .trim pour enlever les espaces avant et après
        let hasError = false;
        const newTitle = this.domTitleEvent.value.trim();
        if( newTitle === '' ) {
            this.domTitleEvent.classList.add( 'error' );
            this.domTitleEvent.value = '';
            hasError = true;
        }

        const newDescription = this.domDescription.value.trim();
        if( newDescription === '' ) {
            this.domDescription.classList.add( 'error' );
            this.domDescription.value = '';
            hasError = true;
        }

        const newStartDate = this.domStartDate.value.trim();
        if( newStartDate === '' ) {
            this.domStartDate.classList.add( 'error' );
            this.domStartDate.value = '';
            hasError = true;
        }
        
        const newEndDate = this.domEndDate.value.trim();
        if( newEndDate === '' ) {
            this.domEndDate.classList.add( 'error' );
            this.domEndDate.value = '';
            hasError = true;
        }

        const newLongitude = this.domLongitude.value.trim();
        if( newLongitude === '' ) {
            this.domLongitude.classList.add( 'error' );
            this.domLongitude.value = '';
            hasError = true;
        }

        const newLatitude = this.domLatitude.value.trim();
        if( newLatitude === '' ) {
            this.domLatitude.classList.add( 'error' );
            this.domLatitude.value = '';
            hasError = true;
        }

        if( hasError ) {
            alert("Vous avez fait une ou plusieurs erreurs lors de la saisie des données, merci de vérifier les rubriques qui s'affichent en rouge");
            return;
        }

        // Création timestamp
        const now = Date.now();

        // Création nouvel événement en version JSON
        const newJson = {
                titleEvent: this.domTitleEvent.value,
                descriptionEvent: this.domDescription.value,
                startDateEvent: this.domStartDate.value,
                endDateEvent: this.domEndDate.value,
                longitudeEvent: this.domLongitude.value,
                latitudeEvent: this.domLatitude.value
        }
        // Vidage des champs
        this.domTitleEvent.value = this.domDescription.value = this.domStartDate.value = this.domEndDate.value = this.domLongitude.value = this.domLatitude.value = '';
        // Fabriquer l'événement
        const newLocalEvt = new LocalEvent( newJson );

        // Enregistrer dans LocalStorage
        this.allEvents.push( newLocalEvt );
        localStorage.setItem( appConfig.localStorageName, JSON.stringify( this.allEvents )); 
    }   
    
/*************************************************************************************************************/
}


const instance = new App();

export default instance;