const mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');

export class LocalEvent {

    titleEvent;
    descriptionEvent;
    startDateEvent;
    endDateEvent;
    longitudeEvent;
    latitudeEvent;
    markerEvent;


    constructor( jsonEvent ) {
        this.titleEvent = jsonEvent.titleEvent;
        this.descriptionEvent = jsonEvent.descriptionEvent;
        this.startDateEvent = jsonEvent.startDateEvent;
        this.endDateEvent = jsonEvent.endDateEvent;
        this.longitudeEvent = jsonEvent.longitudeEvent;
        this.latitudeEvent = jsonEvent.latitudeEvent;
        this.createMarker(); 
    }

    createMarker(){
                // Creation d'un Marker
                // this.markerEvent = new mapboxgl.Marker({ color: this.changeColorMarker(this.startDateEvent) }); 
                this.markerEvent = new mapboxgl.Marker({ color: this.changeColorMarker( this.startDateEvent )}); 
            

                // Création de la popup pour le marker
                const popup = new mapboxgl.Popup();

                // Attribution de la popup au marker
                this.markerEvent.setPopup( popup );
                // Attribution des coordonnées géo
                this.markerEvent.setLngLat({ lng: this.longitudeEvent, lat: this.latitudeEvent });
                //Titre
                this.markerEvent.getElement().title = `${this.titleEvent}\nDu : ${this.startDateEvent} au : ${this.endDateEvent}` ;

                //Bouton Delete
                const btnDelete = '❌';

                // Ajout de texte
                popup.setHTML(`
                    <h2>${this.titleEvent}</h2>
                    <p>${this.descriptionEvent}</p>
                    <p>${this.startDateEvent}</p>
                    <p>${this.endDateEvent}</p>
                    <br>
                    <button type="button" class="delete">${btnDelete}</button>
                `);
    }

    changeColorMarker( startDate ){
        const dateEvent = new Date( startDate );
        const space = dateEvent.getTime() - new Date().getTime();

        let colorMarker = '';
        const limit = 3 * 60 * 60 * 24 * 1000;

        console.log(space - limit);

        if( space > limit) {
            colorMarker = 'green';
        } else {
            if( space <= limit && space >=0 ) {
                colorMarker = 'orange';
            } else {
                colorMarker = 'red';
            }
        }
        return colorMarker;
    }

    // Méthode pour appeler l'événement par JSON.stringify()
    toJSON() {
        return {
            titleEvent: this.titleEvent,
            descriptionEvent: this.descriptionEvent,
            startDateEvent: this.startDateEvent,
            endDateEvent: this.endDateEvent,
            longitudeEvent: this.longitudeEvent,
            latitudeEvent: this.latitudeEvent
        }
    }
}
